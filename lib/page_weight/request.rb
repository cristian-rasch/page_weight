require_relative "response"

module PageWeight
  class Request
    MAX_HISTORY = 0

    attr_reader :logger

    def initialize(url)
      @url = url
      @buffer = StringIO.new
      @logger = Logger.new(@buffer)
      @agent = Mechanize.new
      @agent.max_history = MAX_HISTORY
      @agent.log = @logger
    end

    def get(user_agent)
      @agent.user_agent_alias = user_agent
      begin
        page = @agent.get(@url)
        error = nil
      rescue Mechanize::Error => err
        page = nil
        error = err
      end

      Response.new(page, error, self)
    end

    def log
      @buffer.rewind
      @buffer.read
    end
  end
end

