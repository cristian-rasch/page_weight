module PageWeight
  class Response
    class PartialPage
      def initialize(body_io)
        @body = Nokogiri::HTML(body_io.read)
      end

      def search(*args)
        @body.css(*args)
      end
    end

    CSS_LINKS_SELECTOR = "link[href$=css]".freeze
    JS_LINKS_SELECTOR = "script[src$=js]".freeze
    FAVICON_SELECTOR = "//link[substring(@href, string-length(@href) - string-length('favicon.ico') +1) = 'favicon.ico']".freeze

    attr_reader :errors, :accessed_urls

    def initialize(page, error, request)
      @page = page
      @error = error
      @errors = {}
      if error
        @url = error.url if error.respond_to?(:url)
        @url ||= error.uri.to_s if error.respond_to?(:uri)
        @url ||= error.page.uri.to_s if error.respond_to?(:page)
        @errors[@url] = error.inspect if @url

        @page = error.page if error.respond_to?(:page)
        @page ||= PartialPage.new(error.body_io) if error.respond_to?(:body_io)
      else
        @url = page.uri.to_s
      end

      @accessed_urls = []
      @accessed_urls << @url if @url
      @request = request
      @logger = request.logger
      @requests_count = 1
    end

    def success?
      @error.nil?
    end

    def code
      unless instance_variable_defined?(:@code)
        @code = if success?
                  @page.code
                elsif @error.respond_to?(:response_code)
                  @error.response_code
                elsif @error.respond_to?(:response)
                  @error.response.code
                end
      end

      @code
    end

    def log
      @log ||= @request.log
    end

    def requests_count
      @requests_count ||= accessed_urls.size
    end

    def html_weight
      @page.body.bytesize if @page
    end

    def css_weight
      @css_weight ||= asset_links_weight(:css) + inline_elements_weight("style")
    end

    def js_weight
      @js_weight ||= asset_links_weight(:js) + inline_elements_weight("script")
    end

    def image_weight
      @image_weight ||= begin
                          @page.images.inject(0) do |sum, image|
                            image_url = image.url
                            sum + bytesize_for(image_url)
                          end
                        end
    end

    def favicon_weight
      unless instance_variable_defined?(:@favicon_weight)
        @favicon_weight = 0
        @page.at(FAVICON_SELECTOR)
        if @page && (favicon = @page.at(FAVICON_SELECTOR))
          favicon_url = absolute_url_for(favicon["href"])
          @favicon_weight = bytesize_for(favicon_url)
        end
      end

      @favicon_weight
    end

    def page_weight
      @page_weight ||= html_weight + css_weight + js_weight + image_weight + favicon_weight
    end

    private

    def asset_links_weight(asset_type)
      nodes = case asset_type
              when :css then @page.search(CSS_LINKS_SELECTOR)
              when :js then @page.search(JS_LINKS_SELECTOR)
              else []
              end

      urls = nodes.map { |node| absolute_url_for(node["href"] || node["src"]) }
      urls.inject(0) { |sum, url| sum + bytesize_for(url) }
    end

    def bytesize_for(url)
      @logger.info "Requesting: #{url}"
      @accessed_urls << url
      weight = 0

      begin
        weight = open(url).read.bytesize
      rescue OpenURI::HTTPError => err
        errors[url] = "(#{err.io.status.first}) #{err.message}"
      rescue => err
        errors[url] = err.inspect
      ensure
        @requests_count += 1
      end

      weight
    end

    def absolute_url_for(src)
      case
      when src =~ %r(https?://) then src # absolute URL
      when src =~ %r(\A//) then "#{page_scheme}:#{src}" # schemeless URI
      when src =~ %r(\A/?[^/]) then "#{page_scheme}://#{page_domain}#{src}" # URL relative to the site domain
      else URI.join(url_without_trailing_slash, src).to_s # relative URL relative to the page's parent path
      end
    end

    def url_without_trailing_slash
      @url_without_trailing_slash ||= @url.end_with?("/") ? @url[0...-1] : @url
    end

    def page_domain
      @page_domain ||= begin
                         domain = page_uri.host
                         domain << ":#{page_uri.port}" unless [80, 443].include?(page_uri.port)
                         domain
                       end
    end

    def page_scheme
      @page_scheme ||= page_uri.scheme
    end

    def page_uri
      @uri ||= URI(@url)
    end

    def inline_elements_weight(tag_name)
      @page.search(tag_name).reject { |n| n.text.empty? }.inject(0) do |sum, node|
        sum + node.text.strip.bytesize
      end
    end
  end
end

