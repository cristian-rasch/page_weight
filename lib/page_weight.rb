require "page_weight/version"
require "page_weight/request"
require "mechanize"
require "logger"
require "stringio"
require "open-uri"

module PageWeight
  Mechanize::AGENT_ALIASES.default_proc = proc do |hash, key|
    hash[key] = key
  end
  DEFAULT_USER_AGENT = Mechanize::AGENT_ALIASES.keys.find { |ua| ua =~ /windows chrome/i }.freeze

  class << self
    def for(url, user_agent = DEFAULT_USER_AGENT)
      Request.new(url).get(user_agent)
    end
  end
end
