# PageWeight

This library calculates the total page size, as well as the size of each file category (e.g. HTML, CSS, JS and image files) of any given page.

## Installation

Add this line to your application's Gemfile:

```ruby
gem "page_weight", github: "PureOxygen/pageWeight"
```

And then execute:

    $ bundle

## Usage

```ruby
require "page_weight"

# basic usage
res = PageWeight.for("https://www.google.com/", "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1")
res.page_weight # the total page weight
res.html_weight # the weight of the source HTML code
res.css_weigh # the weight of all inlined styles and linked stylesheets
res.js_weight # the weight of all inlined Javascript code and linked Javascript source files
res.image_weight # the weight of all images embedded in the page
res.favicon_weight # the weight of the favicon icon (if presennt)

# debugging information
res.success? # true if the main page was found, false otherwise
res.code # the HTTP response code
res.requests_count # number of HTTP requests issued
res.accessed_urls # a list of all URL accessed
res.log # a String listing every URL accessed
res.errors # a Hash listing any possible error logged while accessing each of the URLs GET'ed
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

To run the test suite, simple run the bundle exec rake command from your terminal.

## Contributing

1. Fork it ( https://github.com/user/page_weight/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
