require_relative "minitest_helper"

class TestPageWeight < Minitest::Test
  SUCCESS_PAGE = "https://www.google.com/".freeze
  NOT_FOUND_PAGE = "http://www.example.com/not-found".freeze

  def test_it_allows_you_to_freely_set_the_value_of_the_user_agent_header
    assert_predicate ::PageWeight.for(SUCCESS_PAGE, "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53"), :success?
  end

  def test_it_succeeds_in_issuing_the_first_request
    assert_predicate ::PageWeight.for(SUCCESS_PAGE), :success?
  end

  def test_it_exposes_the_response_code_from_the_first_request
    assert_equal "200", ::PageWeight.for(SUCCESS_PAGE).code
    assert_equal "404", ::PageWeight.for(NOT_FOUND_PAGE).code
  end

  def test_it_exposes_an_errors_hash
    response = ::PageWeight.for(NOT_FOUND_PAGE)
    refute_nil response.errors[NOT_FOUND_PAGE]
  end

  def test_it_logs_every_request_made
    refute_empty ::PageWeight.for(SUCCESS_PAGE).log
  end

  def test_it_exposes_the_html_weight
    assert ::PageWeight.for(SUCCESS_PAGE).html_weight > 0
  end

  def test_it_exposes_the_css_weight
    assert ::PageWeight.for(SUCCESS_PAGE).css_weight > 0
  end

  def test_it_exposes_the_js_weight
    assert ::PageWeight.for(SUCCESS_PAGE).js_weight > 0
  end

  def test_it_exposes_the_image_weight
    assert ::PageWeight.for(SUCCESS_PAGE).image_weight > 0
  end

  def test_it_exposes_the_favicon_weight
    assert ::PageWeight.for("https://twitter.com/").favicon_weight > 0
  end

  def test_it_exposes_the_total_weight
    response = ::PageWeight.for(SUCCESS_PAGE)
    assert response.page_weight > response.html_weight
  end

  def test_it_exposes_the_number_of_requests_issued
    response = ::PageWeight.for(SUCCESS_PAGE)
    response.page_weight
    assert response.requests_count > 1
  end

  def test_it_exposes_all_urls_accessed
    response = ::PageWeight.for(SUCCESS_PAGE)
    response.page_weight
    refute_empty response.accessed_urls
  end
end
